package com.cgi.osd.transactiondemo.logic;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.persistence.PersistenceManager;

/**
 * This class is responsible for implementing the booking services.
 *
 */

@ApplicationScoped
public class BookingServiceBean implements BookingService {

    @Inject
    Logger logger;

    @Inject
    private PersistenceManager persistenceManager;

    @Override
    public List<SeatDO> getAllSeats() throws PersistenceOperationException {
	final List<SeatDO> seats = this.persistenceManager.getAllSeats();
	return seats;
    }

}
