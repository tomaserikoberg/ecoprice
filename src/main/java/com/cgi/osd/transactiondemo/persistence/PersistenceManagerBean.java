package com.cgi.osd.transactiondemo.persistence;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for implementing methods for persistence handling. All methods are thread safe.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NEVER)
@Interceptors(ExceptionInterceptor.class)
public class PersistenceManagerBean implements PersistenceManager {

    @Inject
    private EntityManager em;

    @Inject
    private PersistenceObjectFactory objectFactory;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<SeatDO> getAllSeats() {
	final List<Seat> dbResult = fetchAllSeats();
	final List<SeatDO> result = this.objectFactory.createSeatDOList(dbResult);
	return result;
    }

    private List<Seat> fetchAllSeats() {
	final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findAll", Seat.class);
	query.setLockMode(LockModeType.OPTIMISTIC);
	final List<Seat> dbResult = query.getResultList();
	return dbResult;
    }

}
