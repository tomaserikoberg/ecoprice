package com.cgi.osd.transactiondemo.persistence;

import java.util.List;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This interface defines all methods that should be used for persistence operations.
 *
 *
 */
public interface PersistenceManager {

    /**
     * This method returns a list of all free seats.
     *
     * @return a list of free seats. The list might be empty.
     * @throws PersistenceOperationException
     *             when a persistence operation fails.
     */
    List<SeatDO> getAllSeats() throws PersistenceOperationException;

}
