package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

/**
 * This class is responsible for the controller part of the MVC pattern for the booking process.
 *
 *
 */
@SessionScoped
@Named
public class BookingController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private BookingData bookingData;

    public List<SeatDO> getAllSeats() {
	return this.bookingData.getAllSeats();
    }
}
